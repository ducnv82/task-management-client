# Task management application frontend uses Angular 6
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.  

The frontend application should implement the following features:

*   Load the tasks from the backend asynchronously and list them according to their dueDate and the priority
*   If new tasks come in, they should be automatically added to the list of tasks
*   Each task has a detail view, where all information of the task can be edited and saved
*   A task can be postponed, so that it
    
    *   is removed from the list
    *   appears on the list at a later time again


# Prerequisite

Install latest node.js, then run `npm install`

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

# Thought about requirements
Requirement: A task can be postponed, so that it

*   is removed from the list
*   appears on the list at a later time again

so I implement: Postponed tasks are hidden in the frontend app except for its **due date** is **today** (appears on the list at a later time again)

Feel free to discuss the changes (if needed) for requirements.
