import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { EditTaskComponent } from './edit-task/edit-task.component';

const routes: Routes = [
    { path: '', redirectTo: 'task-list', pathMatch: 'full' },
    { path: 'task-list', component: TasksListComponent },
    { path: 'edit-task', component: EditTaskComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
