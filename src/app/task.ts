export class Task {
    id: string;
    title: string;
    description: string;
    priority: string;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    dueDate: Date;
    resolvedAt: Date;
}
