import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private baseUrl = 'http://localhost:8080/api/tasks';

  constructor(private http: HttpClient) { }

  getTask(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updateTask(id: string, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  getTasksList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
