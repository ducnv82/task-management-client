import { Component, OnInit } from '@angular/core';
import {TaskService} from "../task.service";
import {Router} from "@angular/router";
import {Task} from "../task";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {  
  editForm: FormGroup;
  submitted = false;
 
  constructor(private formBuilder: FormBuilder,private router: Router, private taskService: TaskService) {}

  ngOnInit() {
    let taskId = localStorage.getItem("editTaskId");
    if (!taskId) {
      alert("Invalid action.")
      this.router.navigate(['task-list']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      title: [],
      description: [],
      priority: [],
      status: [],
      resolvedAt: ['', Validators.required],
      dueDate: ['', Validators.required],
      createdAt: [],
      updatedAt: []
    });
    this.taskService.getTask(taskId)
      .subscribe(data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.submitted = true;    
    if (this.editForm.invalid) {
        return;
    }
    this.taskService.updateTask(localStorage.getItem("editTaskId"), this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['task-list']);
        },
        error => {
          alert(error);
        });
  }

  get f() { return this.editForm.controls; }
}
