import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../../task';

@Pipe({
    name: 'taskFilterPipe',
    pure: false
})
export class TaskFilterPipe implements PipeTransform {
    transform(items: Task[]): any {
        if (!items) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        let today = new Date();
        return items.filter(item => (item.status !== 'POSTPONED' || (item.status === 'POSTPONED' && item.dueDate != null && new Date(item.dueDate).toDateString() === today.toDateString()))).sort((a: any, b: any) => {
            let dateComparision = new Date(a.dueDate).getTime() - new Date(b.dueDate).getTime();           
            return dateComparision === 0 ? a.priority.localeCompare(b.priority) : dateComparision;
          });
    }
}
