import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import {Router} from "@angular/router";
import * as Stomp from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';

import { TaskService } from '../task.service';
import { Task } from '../task';

@Component({
  selector: 'tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit, OnDestroy {
  
  tasks: Observable<Task[]>;
  private stompClient;  

  constructor(private taskService: TaskService, private router: Router) { }

  ngOnInit() {
    this.reloadData();

    this.subscribeTasksOverWebsocket();    
  }

  subscribeTasksOverWebsocket() {
    const socket = new SockJS('http://localhost:8080/gs-task-websocket');    
    this.stompClient = Stomp.over(socket);
    this.stompClient.reconnect_delay = 1000;
    const _this = this;
    this.stompClient.connect({}, function (frame) {   
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe('/topic/tasks', function (message) {
        console.log('Task id: ' + message.body);
        _this.reloadData();
      });
    });    

    socket.onclose = function() {
      setTimeout(function() {
        _this.subscribeTasksOverWebsocket();
      }, 1000);
    };
  }

  reloadData() {
    this.tasks = this.taskService.getTasksList();
  }

  editTask(task: Task): void {
    localStorage.removeItem("editTaskId");
    localStorage.setItem("editTaskId", task.id.toString());
    this.router.navigate(['edit-task']);
  };

  ngOnDestroy() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
  }
}
